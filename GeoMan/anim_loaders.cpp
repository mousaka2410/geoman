#include <iostream>
#include <fstream>
#include <string>
#include "anim_loaders.h"
#include "load_or_die.h"

using namespace std;

__declspec(deprecated) void loadLinAnim_0_2(LinAnim *anim, const char *filename){
	ifstream inFile(filename);
	AnimFrame frame;
	char inputType;

	memset(&frame, 0, sizeof (AnimFrame));

	bool frame_started = false;
	bool anim_started = false;
	int hotXPlus = 0;
	int hotXAccum = 0;
	int tmpHotY = 0;

	while (inFile >> inputType) {

		cout << inputType << " ";
		switch (inputType) {
			case 'l': { /* lower 'L' */ 
				string imageFile;

				inFile >> imageFile;
				frame.img = load_image_or_die(imageFile.c_str());
				frame.rect.w = frame.img->w;
				frame.rect.h = frame.img->h;
				anim_started = true; }
				break;

			case 'f':
				if (!anim_started) { 
					cerr << "Should specify animation "
						"type first." << endl;
				}
				frame_started = true;
				break;

			case 'r': {
				SDL_Rect *rect = &frame.rect;
				inFile >> rect->x >> rect->y >> rect->w >> rect->h;
				rect->w -= rect->x - 1;
				rect->h -= rect->y - 1;}
				break;
			
			case 'd': 
				inFile >> frame.duration;
				break;

			case 'e':
				if (!frame_started) {
					cerr << "No frame started" << endl;
				}

				anim->insertFrame(frame);
				frame_started = false;
				break;

			default:
				cerr << "Undefined symbol '" << 
					inputType << "'" << endl;
		}
	}
	cout << endl;

	inFile.close();
}

void loadLinAnim_0_3(LinAnim *anim, const char *filename){
	ifstream inFile(filename);
	AnimFrame frame;
	char inputType;
	string imgDir = "";

	memset(&frame, 0, sizeof (AnimFrame));

	bool frame_started = false;
	bool anim_started = false;
	int hotXPlus = 0;
	int hotXAccum = 0;
	int tmpHotY = 0;

	while (inFile >> inputType) {

		cout << inputType << " ";
		switch (inputType) {

			case 'D':
				inFile >> imgDir;
				if (imgDir[imgDir.length()-1] != '/')
					imgDir += '/';
				break;

			case 'f':
				frame_started = true;
				break;

			case 'I':{
				string imgFile;

				inFile >> imgFile;
				frame.img = load_zoom_or_die((imgDir + imgFile).c_str());
				frame.rect.x = 0;
				frame.rect.y = 0;
				frame.rect.w = frame.img->w;
				frame.rect.h = frame.img->h;
				break; }
			
			case 'i':{
				string imgFile;

				inFile >> imgFile;
				frame.img = load_image_or_die((imgDir + imgFile).c_str());
				frame.rect.x = 0;
				frame.rect.y = 0;
				frame.rect.w = frame.img->w;
				frame.rect.h = frame.img->h;
				break; }
			case 'r': {
				SDL_Rect *rect = &frame.rect;
				inFile >> rect->x >> rect->y >> rect->w >> rect->h;
				rect->w -= rect->x - 1;
				rect->h -= rect->y - 1;}
				break;
			
			case 'd': 
				inFile >> frame.duration;
				break;

			case 'e':
				if (!frame_started) {
					cerr << "No frame started" << endl;
				}

				anim->insertFrame(frame);
				frame_started = false;
				break;

			default:
				cerr << "Undefined symbol '" << 
					inputType << "'" << endl;
		}
	}
	cout << endl;

	inFile.close();
}

void loadLinAnim(LinAnim *anim, const char *filename){
	loadLinAnim_0_2(anim, filename);
}