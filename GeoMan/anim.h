#ifndef _ANIM_H
#define _ANIM_H

#include <sdl/sdl.h>
#include <sdl_rotozoom.h>
#include <cstdlib>
#include <memory>

struct AnimFrame {
	SDL_Surface *img;
	SDL_Rect rect;
	double duration;
};

class AnimInstance;

// Knows how to cycle the frames of an animatable
class AnimIterator{
public:
	virtual void firstFrame(void) = 0;
	virtual bool nextFrame(void) = 0;
	virtual void smoothBreak(void) = 0;
	virtual AnimFrame *currentFrame(void) = 0;
};

// Holds the frames of an animation
class Animatable{
public:
	virtual std::auto_ptr<AnimIterator> getIterator(void) = 0;
};

// Is instructed about everything
class AnimInstance {
public:
	AnimInstance(void);
	AnimInstance(Animatable *anim);
	~AnimInstance();
	void setPos(double x, double y);
	void movePos(double dx, double dy);
	void pause(void);
	void unpause(void);
	void smoothBreak(void);
	void reset(void);
	int  process(double dt);
	double getTimeIntoFrame(void);
	void render(SDL_Surface *screen);
	void render(SDL_Surface *screen, int nTurns);

	enum { // process return flags
		animNothing     = 0x0,
		animPaused      = 0x1,
		animBackAtFirst = 0x2,
		animBreaking    = 0x4,
		animNextFrame   = 0x8,
		animBreakingAndBack = animBackAtFirst | animBreaking
	};

private:
	std::auto_ptr<AnimIterator> m_anim;
	double m_x, m_y;
	bool m_paused;
	bool m_breaking;
	double m_timeIntoFrame;

	void resetToFirstFrame(void);
	AnimFrame *getCurrentFrame(void);
	bool nextFrame(void);
};

#endif