#ifndef _LEVELS_H
#define _LEVELS_H
#include "colors.h"

struct Plate{
	int type;
	int x,y;
	int pressed;
	Plate(void) : pressed(false) {;}
	Plate(int iX, int iY, int iType) : x(iX),y(iY),type(iType),pressed(true) {;}
};

struct Level{
	int nPlates;
	Plate plate[10];
	SDL_Color palette[16];
};

void genLevels(void);

extern Level levels[10];

#endif