#ifndef _COLORS_H
#define _COLORS_H
#include <sdl/sdl.h>

extern SDL_Color blue   ;
extern SDL_Color dblue  ;
extern SDL_Color red    ;
extern SDL_Color green  ;
extern SDL_Color alpha  ;
extern SDL_Color beige  ;
extern SDL_Color lgreen ;
extern SDL_Color white  ;
extern SDL_Color brown  ;
extern SDL_Color pink   ;
extern SDL_Color dpurple;
extern SDL_Color yellow ;
extern SDL_Color gray   ;
extern SDL_Color dgreen ;

#endif