#ifndef _ANIMATABLES_H
#define _ANIMATABLES_H

#include "anim.h"
#include <list>
#include <vector>

class LinAnim : public Animatable{
public:
	void insertFrame(AnimFrame const &frame);
	std::auto_ptr<AnimIterator> getIterator(void);

private:
	std::vector<AnimFrame> m_frames;

	class Iterator : public AnimIterator{
	public:
		Iterator(LinAnim *anim);
		void firstFrame(void); 
		bool nextFrame(void);
		void smoothBreak(void);
		AnimFrame *currentFrame(void);
	private:
		LinAnim *m_anim;
		int m_currentFrame;
	};
};


class PendAnim : public Animatable{
public:
	__declspec(deprecated) void setImage(SDL_Surface *img);
	__declspec(deprecated) void insertForthFrame (SDL_Rect *rect, 
		double duration,
		int hotX, int hotY);
	__declspec(deprecated) void insertBackFrame (SDL_Rect *rect,
		double duration,
		int hotX, int hotY);


private:
	std::vector <AnimFrame> m_forthFrames;
	std::vector <AnimFrame> m_backFrames;
	SDL_Surface *img;
};


#endif