#ifndef _LOAD_OR_DIE_H
#define _LOAD_OR_DIE_H

#include <sdl/sdl_image.h>
#include <sdl/sdl_mixer.h>
#include <sdl_rotozoom.h>
#include <sdl/sdl_ttf.h>

Mix_Music *load_music_or_die(const char *filename);
SDL_Surface *load_image_or_die(const char *filename);
SDL_Surface *load_zoom_or_die(const char *filename);
TTF_Font *load_font_or_die(const char *filename, int ptSize);

#endif