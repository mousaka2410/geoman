#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <ctime>
#include <sdl/sdl.h>
#include <sdl/sdl_ttf.h>
#include <sdl_gfxPrimitives.h>
#include "utils.h"
#include "anim.h"
#include "animatables.h"
#include "load_or_die.h"
#include "anim_loaders.h"
#include "levels.h"
#include "colors.h"

using namespace std;

const double dt = 0.01;
double gettime (void){
	return SDL_GetTicks()/1000.0;
}

const int scrw = 640;
const int scrh = 480;

inline int colorToInt(SDL_Color &c){
	return (c.r << 16) | (c.g << 8) | c.b;
}
inline int colorToInt2(SDL_Color &c){
	return (c.r << 24) | (c.g << 16) | (c.b << 8) | 0xFF;
}

int level;
SDL_Color *palette;

TTF_Font  *fntMono;

LinAnim walk, walk45, slash, slash45, bslash, bslash45;

struct Man{
	AnimInstance walk;
	AnimInstance walk45;
	AnimInstance slash;
	AnimInstance slash45;
	AnimInstance bslash;
	AnimInstance bslash45;
	int vx, vy;
	double x,y;
	double speed;
	int direction;
	bool slashing;
	bool bslashing;
} man;



const int max_figures = 12;
int nFigures = 0;
struct Figure{
	int type;
	double x,y;
	double vx,vy;
	bool visible;
	Figure(void) : visible(false) {;}
} figures[max_figures];

SDL_Surface *figureImage[10];

void drawPlayfield(SDL_Surface *dst, SDL_Rect *playfield, int borderwidth);
void drawText(SDL_Surface *dst, SDL_Rect *pos, SDL_Color *col, char *format, ...);
void drawCash(SDL_Surface *dst, int cash);
void drawMan(SDL_Surface *dst);
void makeLevels(void);
void prosessFigures(double dt);
void drawFigures(SDL_Surface *dst);
void keyDown(SDLKey key);
void keyUp(SDLKey key);
void initMan(void);
void resetMan(void);
void loadImages(void);
int getDirection(int vx, int vy);

#ifdef __cplusplus
extern "C"{
#endif
int main (int argc, char *argv[]) {
	
	SDL_Init(SDL_INIT_VIDEO);
	atexit(SDL_Quit);
	IMG_Init(IMG_INIT_PNG);
	atexit(IMG_Quit);
	TTF_Init();
	atexit(TTF_Quit);

	fntMono = load_font_or_die("fonts/joystix.ttf", 24);
	
	loadImages();
	initMan();
	genLevels();
	level = 0;
	palette = levels[0].palette;

	SDL_Surface *screen;
	screen = SDL_SetVideoMode (scrw, scrh, 32, /*SDL_FULLSCREEN |*/ SDL_SWSURFACE | SDL_DOUBLEBUF);
	if (!screen) {
		cerr << "Could not initiate screen" << endl;
		exit (EXIT_FAILURE);
	}
	
	SDL_WM_SetCaption ("GeoMan", NULL);

	SDL_Rect playfieldrect;
	playfieldrect.x = 10;
	playfieldrect.y = 40;
	playfieldrect.w = scrw - 2*playfieldrect.x;
	playfieldrect.h = scrh - playfieldrect.y-10;
	SDL_Surface *playfield = create_surface(playfieldrect.w, playfieldrect.h);

	int borderwidth = 3;
	
	int cash = 0;
	
	srand(time(NULL));

	double curr_time, prev_time = gettime();
	SDL_Event ev;
	for (;;){
		while (SDL_PollEvent(&ev)){
			switch (ev.type) {
				case SDL_KEYDOWN: keyDown(ev.key.keysym.sym); break;
				case SDL_KEYUP: keyUp(ev.key.keysym.sym); break;
				case SDL_QUIT: exit (EXIT_SUCCESS);
			}
		}
		curr_time = gettime();
		if (curr_time < prev_time) prev_time = curr_time;
		if (curr_time - prev_time > dt){
			SDL_FillRect(screen, NULL, 0);
			
			drawPlayfield(screen, &playfieldrect, borderwidth);
			drawCash(screen, cash);
			
			prosessFigures(dt);
			drawFigures(screen);
			
			if (man.vx != 0 || man.vy != 0) {
				man.walk.unpause();
				man.walk45.unpause();
				man.x += man.speed * man.vx * dt;
				man.y += man.speed * man.vy * dt;
			}

			man.walk.process(dt);
			man.walk45.process(dt);
			if (man.slashing){
				int prf;
				man.slash.process(dt);
				prf = man.slash45.process(dt);
				if (prf & AnimInstance::animBackAtFirst){
					man.slashing = false;
					man.slash.reset();
					man.slash45.reset();
				}
			}
			if (man.bslashing){
				int prf;
				man.bslash.process(dt);
				prf = man.bslash45.process(dt);
				if (prf & AnimInstance::animBackAtFirst){
					man.bslashing = false;
					man.bslash.reset();
					man.bslash45.reset();
				}
			}
			man.walk.setPos(man.x, man.y);
			man.walk45.setPos(man.x, man.y);
			man.slash.setPos(man.x, man.y);
			man.slash45.setPos(man.x, man.y);
			man.bslash.setPos(man.x, man.y);
			man.bslash45.setPos(man.x, man.y);

			int direction = getDirection(man.vx, man.vy);
			if (direction >= 0) man.direction = direction;
			else {man.walk.reset(); man.walk.pause(); man.walk45.reset(); man.walk45.pause();}
			// else man.direction is what it already is
			if (man.direction % 2 == 0) {
				if (man.slashing) man.slash.render(screen, man.direction/2);
				if (man.bslashing) man.bslash.render(screen, man.direction/2);
				man.walk.render(screen, man.direction/2);
			}
			if (man.direction % 2 == 1) {
				if (man.slashing) man.slash45.render(screen, man.direction/2);
				if (man.bslashing) man.bslash45.render(screen, man.direction/2);
				man.walk45.render(screen, man.direction/2);
			}

			prev_time += dt;
			SDL_Flip (screen);
		}
	}

	return 0;
}

#ifdef __cplusplus
}
#endif

void drawPlayfield(SDL_Surface *dst, SDL_Rect *pl, int bw) {
	SDL_FillRect(dst, pl, colorToInt(beige));
	roundedRectangleColor(dst, pl->x - bw, pl->y - bw,
		pl->x + pl->w-1 + bw, pl->y + pl->h-1 + bw, 1, colorToInt2(palette[0]));
	rectangleColor(dst, pl->x - bw+1, pl->y - bw+1,
		pl->x + pl->w-1 + bw-1, pl->y + pl->h-1 + bw-1, colorToInt2(palette[1]));
	rectangleColor(dst, pl->x - bw+2, pl->y - bw+2,		
		pl->x + pl->w-1 + bw-2, pl->y + pl->h-1 + bw-2, colorToInt2(palette[0]));
}

void drawText(SDL_Surface *dst, SDL_Rect *pos, SDL_Color col, char *format, ...){
	char buf[1024];
	va_list args;
	va_start(args, format);
	vsnprintf(buf, 1024, format, args);
	va_end(args);
	SDL_Surface *text = TTF_RenderText_Blended(fntMono, buf, col);
	SDL_BlitSurface(text, NULL, dst, pos);
	SDL_FreeSurface(text);
}

void drawCash(SDL_Surface *dst, int cash){
	SDL_Rect cashpos; int tmpw,tmph;
	TTF_SizeText(fntMono, "$1,000,000 ", &tmpw, &tmph);
	cashpos.y = 0;
	cashpos.x = scrw - tmpw;
	if (cash<1000) drawText(dst, &cashpos, green, "$%d", cash);
	else if (cash<1000000) drawText(dst, &cashpos, green, "$%d,%03d", cash/1000, cash%1000);
	else drawText(dst, &cashpos, green, "$%d,%03d,%03d", cash/1000000, (cash/1000)%1000, cash%1000);
}

void prosessFigures(double dt){
	static double timeSinceLastSpawn = 0.0;
	timeSinceLastSpawn += dt;
	int lastInvisible = max_figures;
	for (int i=0; i<max_figures; i++){
		Figure *f = &figures[i];
		if (f->visible){
			f->x += f->vx;
			f->y += f->vy;
		} else lastInvisible = i;
	}
	if (lastInvisible < max_figures){
		if (timeSinceLastSpawn >= 3.0){
			timeSinceLastSpawn = 0.0;
			Figure *f = &figures[lastInvisible];
			f->type = rand() % 5;
			f->vx = 0.0;
			f->vy = 0.5;
			f->x  = 200;
			f->y  = 0;
			f->visible = true;
		}
	}
}

void drawFigures(SDL_Surface *dst){
	for (int i=0; i<max_figures; i++){
		Figure *f = &figures[i];
		if (f->visible){
			SDL_Rect dstrect = {f->x,f->y,0,0};
			SDL_BlitSurface(figureImage[f->type],NULL,dst,&dstrect);
		}
	}
}

void keyDown(SDLKey key){
	printf("The %s key was pressed!\n",
	SDL_GetKeyName(key));
					
	switch (key){
	case SDLK_ESCAPE: exit(EXIT_SUCCESS); break;
	case SDLK_c:
		// swing sword
		if (!man.bslashing){
			man.slashing = true;
		}
		break;
	case SDLK_x:
		// swing sword backwards
		if (!man.slashing){
			man.bslashing = true;	
		}
		break;
	case SDLK_LEFT:  man.vx--; break;
	case SDLK_RIGHT: man.vx++; break;
	case SDLK_UP:    man.vy--; break;
	case SDLK_DOWN:  man.vy++; break;
	}
}

void keyUp(SDLKey key){					
	printf("The %s key was released!\n", SDL_GetKeyName(key));			
	switch (key){	
	case SDLK_LEFT:  man.vx++; break;
	case SDLK_RIGHT: man.vx--; break;
	case SDLK_UP:    man.vy++; break;
	case SDLK_DOWN:  man.vy--; break;
	}
}
void initMan(void){
	loadLinAnim_0_3(&walk, "walk.txt");
	loadLinAnim_0_3(&walk45, "walk45.txt");
	loadLinAnim_0_3(&slash, "slash.txt");
	loadLinAnim_0_3(&slash45, "slash45.txt");
	loadLinAnim_0_3(&bslash, "slashb.txt");
	loadLinAnim_0_3(&bslash45, "slashb45.txt");
	man.walk = AnimInstance(&walk);
	man.walk45 = AnimInstance(&walk45);
	man.slash = AnimInstance(&slash);
	man.slash45 = AnimInstance(&slash45);
	man.bslash = AnimInstance(&bslash);
	man.bslash45 = AnimInstance(&bslash45);
	resetMan();
}
void resetMan(void){
	man.vx = 0; man.vy = 0;
	man.direction = 0;
	man.x = 100; man.y = 100;
	man.speed = 100;
	man.walk.setPos(man.x, man.y);
	man.walk45.setPos(man.x, man.y);
	man.slashing = false;
	man.bslashing = false;
}
void loadImages(void){
	figureImage[0] = load_image_or_die("gfx/shape2.png");
	figureImage[1] = load_image_or_die("gfx/shape3.png");
	figureImage[2] = load_image_or_die("gfx/shape4.png");
	figureImage[3] = load_image_or_die("gfx/shape5.png");
	figureImage[4] = load_image_or_die("gfx/shape6.png");
}
int getDirection(int vx, int vy){
	int direction = -1;
	if		(vx ==  0 && vy == -1) direction = 0;
	else if (vx == -1 && vy == -1) direction = 7;
	else if (vx == -1 && vy ==  0) direction = 6;
	else if (vx == -1 && vy ==  1) direction = 5;
	else if (vx ==  0 && vy ==  1) direction = 4;
	else if (vx ==  1 && vy ==  1) direction = 3;
	else if (vx ==  1 && vy ==  0) direction = 2;
	else if (vx ==  1 && vy == -1) direction = 1;
	return direction;
}