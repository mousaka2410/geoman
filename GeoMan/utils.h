#ifndef _UTILS_H
#define _UTILS_H

#include <sdl/sdl.h>

/* Create a 32-bit surface with the bytes of each pixel in R,G,B,A order,
   as expected by OpenGL for textures */
SDL_Surface *create_surface(int width, int height);

#endif