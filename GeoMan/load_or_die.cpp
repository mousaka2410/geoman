#include <iostream>
#include "load_or_die.h"

#define printerror(x) \
	if (true) \
		{std::cerr << #x << ": " << x##_GetError () << std::endl;} \
	else (void) 0

Mix_Music *load_music_or_die (const char *filename){
	Mix_Music *music = Mix_LoadMUS (filename);
	if (!music) {
		printerror (Mix);
		exit (EXIT_FAILURE);
	}
	return music;
}

SDL_Surface *load_image_or_die (const char *filename){
	SDL_Surface *image, *tmp;
	image = IMG_Load (filename);
	if (!image) {
		printerror (IMG);
		IMG_Quit ();
		exit (EXIT_FAILURE);
	}
	tmp = zoomSurface(image, 1.0,1.0, SMOOTHING_OFF);
	SDL_FreeSurface(image);
	image = tmp;
	return image;
}

SDL_Surface *load_zoom_or_die (const char *filename){
	SDL_Surface *image, *tmp;
	image = IMG_Load (filename);
	if (!image) {
		printerror (IMG);
		IMG_Quit ();
		exit (EXIT_FAILURE);
	}

	tmp = zoomSurface(image, 2.0,2.0, SMOOTHING_OFF);
	SDL_FreeSurface(image);
	image = tmp;

	return image;
}

TTF_Font *load_font_or_die(const char *filename, int ptSize){
	TTF_Font *font;
	font = TTF_OpenFont(filename, ptSize);
	if (!font) {
		printerror(TTF);
		exit(EXIT_FAILURE);
	}
	return font;
}