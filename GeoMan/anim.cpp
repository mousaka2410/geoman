#include "anim.h"
#include <cassert>

// public functions

AnimInstance::AnimInstance(Animatable *anim) {
	m_anim = anim->getIterator();
	m_paused = false;
	m_breaking = false;
	m_timeIntoFrame = 0.0;
}

AnimInstance::AnimInstance(void) {
	m_paused = false;
	m_breaking = false;
	m_timeIntoFrame = 0.0;
}

AnimInstance::~AnimInstance(void){;}

void AnimInstance::setPos(double x, double y) {
	m_x = x; m_y = y;
}

void AnimInstance::movePos(double dx, double dy) {
	m_x += dx; m_y += dy;
}

void AnimInstance::pause(void) {
	m_paused = true;
}

void AnimInstance::unpause(void) {
	m_paused = false;
}

void AnimInstance::smoothBreak(void) {
	m_breaking = true;
}

void AnimInstance::reset(void) {
	resetToFirstFrame();
	m_timeIntoFrame = 0.0;
}

int AnimInstance::process(double dt) {
	if (m_paused) return animPaused;

	int flags = animNothing;
	if (m_breaking) flags |= animBreaking;

	m_timeIntoFrame += dt;
	double frameDuration = getCurrentFrame()->duration;
	if (m_timeIntoFrame > frameDuration) {
		flags |= animNextFrame;
		m_timeIntoFrame -= frameDuration;
		bool backAtFirst = nextFrame();
		if (backAtFirst) flags |= animBackAtFirst;
	}
	return flags;
}

double AnimInstance::getTimeIntoFrame(void) {
	return m_timeIntoFrame;
}

void AnimInstance::render(SDL_Surface *screen) {
	AnimFrame *frame = getCurrentFrame();
	SDL_Rect *srcRect = &frame->rect;
	SDL_Rect dstRect;
	dstRect.x = m_x;
	dstRect.y = m_y;
	int res = SDL_BlitSurface (frame->img, srcRect, screen, &dstRect);
	assert (res == 0);
}

void AnimInstance::render(SDL_Surface *screen, int nTurns) {
	AnimFrame *frame = getCurrentFrame();
	SDL_Rect dstRect;
	dstRect.x = m_x;
	dstRect.y = m_y;
	SDL_Surface *tmp;
	tmp = rotozoomSurface(frame->img, -90.0*nTurns, 1.0, SMOOTHING_OFF);
	SDL_Rect srcRect = {0, 0, tmp->w, tmp->h};
	int res = SDL_BlitSurface (tmp, &srcRect, screen, &dstRect);
	SDL_FreeSurface(tmp);
	assert (res == 0);
}

// private functions

void AnimInstance::resetToFirstFrame(void) {
	m_anim->firstFrame();
}

AnimFrame *AnimInstance::getCurrentFrame(void) {
	return m_anim->currentFrame();
}

bool AnimInstance::nextFrame(void) {
	return m_anim->nextFrame();
}
