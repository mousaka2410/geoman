#include "animatables.h"
#include <cassert>

//-------------------------------------------------------
// LinAnim

void LinAnim::insertFrame(const AnimFrame &frame){
	m_frames.push_back(frame);
}

std::auto_ptr<AnimIterator> LinAnim::getIterator(void){
	return std::auto_ptr<AnimIterator>(new Iterator(this));
}

LinAnim::Iterator::Iterator(LinAnim *anim){
	m_anim = anim;
	firstFrame();
}

AnimFrame *LinAnim::Iterator::currentFrame(void){
	return &m_anim->m_frames[m_currentFrame];
}

bool LinAnim::Iterator::nextFrame(void){
	m_currentFrame++;
	if (m_currentFrame == m_anim->m_frames.size()) {
		firstFrame();
		return true;
	}
	return false;
}

void LinAnim::Iterator::firstFrame(void){
	m_currentFrame = 0;
}

void LinAnim::Iterator::smoothBreak(void){
	// Do nothing
}

//-------------------------------------------------------
// PendAnim

