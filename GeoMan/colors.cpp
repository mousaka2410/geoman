#include "colors.h"

SDL_Color blue = {0,0,255,0};
SDL_Color dblue = {0,0,127,0};
SDL_Color red  = {255,0,0,0};
SDL_Color green = {0,255,0,0};
SDL_Color alpha = {0,0,0,255};
SDL_Color beige = {255,157,43,0};
SDL_Color lgreen = {64,255,70,0};
SDL_Color white = {255,255,255,0};
SDL_Color brown = {162,93,3,0};
SDL_Color pink = {255,74,211,0};
SDL_Color dpurple = {55,0,72,0};
SDL_Color yellow = {245,255,30,0};
SDL_Color gray = {134,100,100,0};
SDL_Color dgreen = {3,130,0,0};