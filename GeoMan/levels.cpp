#include "levels.h"

Level levels[10];

void genLevels(void){
	Level *lvl;
	lvl = &levels[0];
	lvl->palette[0] = blue;
	lvl->palette[1] = red;
	lvl->palette[2] = beige;
	lvl->nPlates = 1;
	lvl->plate[0] = Plate(200, 200, 1); // (x, y, type)

	lvl = &levels[1];
	lvl->palette[0] = red;
	lvl->palette[1] = white;
	lvl->palette[2] = lgreen;
	lvl->nPlates = 3;
	lvl->plate[0] = Plate(145, 345, 1);
	lvl->plate[1] = Plate(465, 345, 1);
	lvl->plate[2] = Plate(305, 105, 1);

	lvl = &levels[2];
	lvl->palette[0] = white;
	lvl->palette[1] = blue;
	lvl->palette[2] = pink;
	lvl->nPlates = 2;
	lvl->plate[0] = Plate(465, 225, 1);
	lvl->plate[1] = Plate(145, 255, 2);

	lvl = &levels[3];
	lvl->palette[0] = green;
	lvl->palette[1] = red;
	lvl->palette[2] = brown;
	lvl->nPlates = 4;
	lvl->plate[0] = Plate(5, 5, 1);
	lvl->plate[1] = Plate(605, 5, 2);
	lvl->plate[2] = Plate(5, 445, 2); 
	lvl->plate[3] = Plate(605, 445, 1);

	lvl = &levels[4];
	lvl->palette[0] = white;
	lvl->palette[1] = pink;
	lvl->palette[2] = blue;
	lvl->nPlates = 6;
	lvl->plate[0] = Plate(5, 5, 1); 
	lvl->plate[1] = Plate(5, 225, 1); 
	lvl->plate[2] = Plate(5, 445, 1); 
	lvl->plate[3] = Plate(305, 105, 2);
	lvl->plate[4] = Plate(305, 345, 2);
	lvl->plate[5] = Plate(605, 225, 3);

	lvl = &levels[5];
	lvl->palette[0] = brown;
	lvl->palette[1] = blue;
	lvl->palette[2] = gray;
	lvl->nPlates = 5;
	lvl->plate[0] = Plate(145, 105, 2); 
	lvl->plate[1] = Plate(465, 105, 2); 
	lvl->plate[2] = Plate(145, 345, 2); 
	lvl->plate[3] = Plate(465, 345, 2); 
	lvl->plate[4] = Plate(305, 225, 3); 

	lvl = &levels[1];
	lvl->palette[0] = red;
	lvl->palette[1] = beige;
	lvl->palette[2] = yellow;
	lvl->nPlates = 2;
	lvl->plate[0] = Plate(305, 205, 3);
	lvl->plate[1] = Plate(305, 445, 3);

	lvl = &levels[7];
	lvl->palette[0] = gray;
	lvl->palette[1] = yellow;
	lvl->palette[2] = dpurple;
	lvl->nPlates = 1;
	lvl->plate[0] = Plate(305, 225, 4); 
}