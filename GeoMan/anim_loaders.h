#ifndef _ANIM_LOADERS_H
#define _ANIM_LOADERS_H

#include "animatables.h"

void loadLinAnim(LinAnim *anim, const char *filename);
void loadLinAnim_0_3(LinAnim *anim, const char *filename);

#endif